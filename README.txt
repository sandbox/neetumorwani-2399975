Admin Notify
------------

This is a module written by Ebizon Technologies (http://www.ebizontek.com). Contact info@ebizontek.com for any suggestion.

Description
-----------

Admin Notify is a simple module that permits admin to recieve emails whenever a new content is posted on the website. We can configure the content types and also the email on which to recieve email. The email subject and body of the email can be configured as well.

Install
-------

1. Copy the "admin_notify" directory under "modules/" or "sites/all/modules";
2. Go to "admin/modules" and eneble "Admin Notify" module;
3. Go to "admin/config/notify/admin_notify" and configure the settings.

And you are ready to go!
